﻿#include <iostream>
#include <filesystem>
#include "dcmtk/dcmimgle/dcmimage.h"
#include "dcmtk/dcmimage/diregist.h"
#include <opencv2/opencv.hpp>
#include <opencv2/core.hpp>
#include <opencv2/viz.hpp>

namespace stdfs = std::filesystem;
using namespace std;
using namespace cv;
#pragma comment(lib, "ws2_32.lib")
Mat prepare_image(string file)
{
    Mat dst;
    DicomImage* image = new DicomImage(file.c_str());
    int nWidth = image->getWidth();
    int nHeight = image->getHeight();
    int depth = image->getDepth();
    image->setWindow(340, 400);
    const DiPixel* inter= image->getInterData();
    Sint16* arr= (Sint16*)(inter->getRepresentation());
    int z = inter->getCount();
    if (image != NULL)
    {
        if (image->getStatus() == EIS_Normal)
        {
            Uint16* pixelData = (Uint16*)(image->getOutputData(16));
            if (pixelData != NULL)
            {
                 
                dst = Mat(nHeight, nWidth, CV_16UC1, pixelData);
                
            }
        }
        else
            cerr << "Error: cannot load DICOM image (" << DicomImage::getString(image->getStatus()) << ")" << endl;
    }
    return dst;
}

int main()
{
    Mat dst;
    vector<string> files;
    stdfs::path path = "ANONIM2";
    const stdfs::directory_iterator end{};
    for (stdfs::directory_iterator iter{ path }; iter != end; ++iter)
        files.push_back(string(iter->path().string()));
    string file = files[0];
    DicomImage* image = new DicomImage(file.c_str());
    int nWidth = image->getWidth();
    int nHeight = image->getHeight();
    int depth = files.size();
    int dims[] = { nHeight,nWidth , depth }; 
    int x = 146;
    int y = 100;
    int width = 140; //
    int height = 140; //
    Mat cloud(height, width, CV_32FC3);  
    for (int r = 0; r < height; r++)
        for (int c = 0; c < width; c++) {
            Vec3f point(r, c);
            cloud.at<Vec3f>(r, c) = point;
        }
    Vec3b black = { 0,0,0 };
        viz::Viz3d viz(" mywindow");
        int j = 0;
        while (j < files.size()) {
            Mat dst = prepare_image(files[j]);
            Mat dst_roi(dst, Rect(x, y, width, height));
            Mat masked_cloud = cloud.clone(); 
            for (int r = 0; r < height; r++)
                for (int c = 0; c < width; c++)
                    if (dst_roi.at<short int>(r, c) == 0)
                        masked_cloud.at<Vec3f>(r, c) = Vec3f(NULL, NULL, NULL);
            string s = to_string(j);
            viz.showWidget(s, cv::viz::WCloud::WCloud(masked_cloud, viz::Color::white()),
                Affine3d().translate(Vec3d(0.0, 0.0, j)));
            j++;
        }
        viz.spin();     
}
